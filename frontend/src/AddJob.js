import React, { Component } from 'react';
import Form from './components/Form';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Header from "./components/Header";

class AddJob extends Component {

    constructor(props) {
        super(props);
        this.state = {
            jobs: [],
            isLoaded: false
        }
    }

    state = {
        fields:{}
    }
    onSubmit = fields => {
        this.setState({fields});
    };

    render() {
        return (
            <div className="App">
                <Header/>
                <Container>
                    <Row>
                        <div className={"col-md-12"}>
                            <h2>Add Jobs</h2>
                            <Form onSubmit={fields =>this.onSubmit(fields)} />
                        </div>
                    </Row>
                </Container>

            </div>
        );
    }
}

export default AddJob;