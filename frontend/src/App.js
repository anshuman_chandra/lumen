import React, { Component } from 'react';
import Form from './components/Form';
import Jobs from './Jobs';
import Row from "react-bootstrap/Row";
import Container from "react-bootstrap/Container";
import Button from "reactstrap/es/Button";
import Header from "./components/Header";

class App extends Component {

    constructor(props) {
        super(props);
        this.state = {
            jobs: [],
            isLoaded: false
        }
    }

    componentDidMount() {
        fetch('http://localhost:8888/backend/public/job')
            .then(res => res.json())
            .then(json => {
                this.setState({
                    isLoaded:true,
                    jobs: json
                })
            });
    }

    state = {
        fields:{}
    }
    onSubmit = fields => {
        this.setState({fields});
    };

    render() {
        var {isLoaded, jobs} = this.state;
        if(!isLoaded) {
            return (
            <div>Loading...</div>
        );
        } else {
            return (
                <div className="App">
                    <nav className={"navbar navbar-expand-lg bg-secondary fixed-top text-uppercase"} id={"mainNav"}>
                        <Container>
                            <a className={"navbar-brand js-scroll-trigger"} href={"#page-top"}> Home </a>
                            <Button className={"navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded"}>Home</Button>
                            <div className="collapse navbar-collapse" id="navbarResponsive">
                                <ul className="navbar-nav ml-auto">
                                    <li className="nav-item mx-0 mx-lg-1">
                                        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                                           href="/">Jobs</a>
                                    </li>
                                    <li className="nav-item mx-0 mx-lg-1">
                                        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                                           href="/addJob">Add Job</a>
                                    </li>
                                    <li className="nav-item mx-0 mx-lg-1">
                                        <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                                           href="#contact">Contact</a>
                                    </li>
                                </ul>
                            </div>
                        </Container>
                    </nav>
                    <Header/>
                    <Container>
                        <Row>
                            <div className={"col-md-12"}>
                                <h2>Jobs</h2>
                                <Jobs jobs={jobs}/>
                            </div>
                        </Row>
                    </Container>
                    <Container>
                        <Row>
                            <div className={"col-md-12"}>
                                <h2>Add Job</h2>
                            <Form/>
                            </div>
                        </Row>
                    </Container>
                </div>
            );
        }
    }
}

export default App;
