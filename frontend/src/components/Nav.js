import React from "react";
import Container from "../App";
import Button from "reactstrap/es/Button";

class Nav extends React.Component {
    render() {
        return(
            <nav className={"navbar navbar-expand-lg bg-secondary fixed-top text-uppercase"} id={"mainNav"}>
                <Container>
                    <a className={"navbar-brand js-scroll-trigger"} href={"#page-top"}> Home </a>
                    <Button className={"navbar-toggler navbar-toggler-right text-uppercase bg-primary text-white rounded"}>Home</Button>
                    <div className="collapse navbar-collapse" id="navbarResponsive">
                        <ul className="navbar-nav ml-auto">
                            <li className="nav-item mx-0 mx-lg-1">
                                <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                                   href="#portfolio">Jobs</a>
                            </li>
                            <li className="nav-item mx-0 mx-lg-1">
                                <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                                   href="#about">Add Job</a>
                            </li>
                            <li className="nav-item mx-0 mx-lg-1">
                                <a className="nav-link py-3 px-0 px-lg-3 rounded js-scroll-trigger"
                                   href="#contact">Contact</a>
                            </li>
                        </ul>
                    </div>
                </Container>
            </nav>
        );

    }
}

export default Nav;