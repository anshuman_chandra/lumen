import React from 'react';
import axios from 'axios';

class Form extends React.Component {
    constructor(props) {
        super(props);
        this.updateState = this.updateState.bind(this);
    }

    state = {
        title: '',
        description: '',
        category: '',
        location: '',
        active: 1,
        id: ''
    }
     updateState(row){
        this.setState({
            title: row.title,
            description: row.description,
            category: row.category,
            location: row.location,
            id: row.id
        });
    }
    change = e => {
        this.setState({
           [e.target.name]: e.target.value
        });
    };

    componentDidMount() {
    }

    onSubmit = e => {
        e.preventDefault();
        console.log(JSON.stringify(this.state, null, 2));
        axios({
            method: 'POST',
            url: `http://localhost:8888/backend/public/addJob`,
            headers: {
                'content-type': 'application/json',
                 'Access-Control-Allow-Origin': '*'
            },
            data: JSON.stringify(this.state, null, 2)
            //data: this.state
        })
            .then(result => {
                console.log(result.data);
            })
        //this.props.onSubmit(this.state)
        this.setState({
            title: '',
            description: '',
            category: '',
            location: ''
        });
    };

    render() {
        return(
            <form>
                <div className="control-group">
                    <div className="form-group floating-label-form-group controls mb-0 pb-2">
                        <label>Title</label>
                <input
                    name = 'title'
                    className="form-control"
                    placeholder='Job Title'
                    value={this.state.title}
                    onChange={e => this.change(e)}
                />
                    </div>
                </div>
                <br />
                <div className="control-group">
                    <div className="form-group floating-label-form-group controls mb-0 pb-2">
                        <label>Title</label>
                <input
                    name='description'
                    className="form-control"
                    placeholder='Description'
                    value={this.state.description}
                    onChange={e => this.change(e)}
                />
                    </div>
                </div>
                <br />
                <div className="control-group">
                    <div className="form-group floating-label-form-group controls mb-0 pb-2">
                        <label>Title</label>
                <input
                    name='category'
                    className="form-control"
                    placeholder='Category'
                    value={this.state.category}
                    onChange={e => this.change(e)}
                />
                    </div>
                </div>
                <br />
                <div className="control-group">
                    <div className="form-group floating-label-form-group controls mb-0 pb-2">
                        <label>Title</label>
                <input
                    name= 'location'
                    className="form-control"
                    placeholder='Location'
                    value={this.state.location}
                    onChange={e => this.change(e)}
                />
                    </div>
                </div>
                <br />
                <div className="form-group">
                    <button onClick={e => this.onSubmit(e)} type="submit" className="btn btn-primary btn-xl" id="sendMessageButton">Send</button>
                </div>
            </form>
        );

    }
}

export default Form;