import React from 'react';
import { Route, IndexRoute } from 'react-router';
import App from './App';
import AddJob from './AddJob';

export default (
    <Route path="/" component={App}>
        <IndexRoute component={App} />
        <Route path="/AddJob" component={AddJob} />
    </Route>
);