import React from 'react';
import {BootstrapTable, TableHeaderColumn} from 'react-bootstrap-table';


class Jobs extends React.Component {

    constructor(props) {
        super(props);
        this.editJob = this.editJob.bind(this);
    }

    imgFormatter(cell,row) {
        console.log(row);
    return (
        <div>
            <button onClick={this.editJob(row)} value={row}> EDIT </button>
        </div>
    );
    }

    editJob = row => e => {
        e.preventDefault();
        this.props.updateState(row);
    }

    render() {
        const columns = [{
            dataField: 'id',
            text: 'Job ID'
        }, {
            dataField: 'title',
            text: 'Title'
        }, {
            dataField: 'description',
            text: 'Description'
        }, {
            dataField: 'location',
            text: 'Location'
        }, {
            dataField: 'category',
            text: 'Category'
        }

        ];

        return(
            <BootstrapTable keyField='id' data={ this.props.jobs } columns={ columns }>
                <TableHeaderColumn dataField='title'>Job Title</TableHeaderColumn>
                <TableHeaderColumn dataField='description'>Description</TableHeaderColumn>
                <TableHeaderColumn dataField='category'>Category</TableHeaderColumn>
                <TableHeaderColumn dataField='location'>Location</TableHeaderColumn>
                <TableHeaderColumn dataField='edit' dataFormat={ this.imgFormatter.bind(this) }>Edit</TableHeaderColumn>
            </BootstrapTable>

        );

    }
}

export default Jobs;