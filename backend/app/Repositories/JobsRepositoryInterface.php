<?php
/**
 * Created by PhpStorm.
 * User: anshumanc
 * Date: 2019-04-05
 * Time: 11:46
 */

namespace App\Repositories;


interface JobsRepositoryInterface
{

    public function selectAll();

    public function find($id);
}