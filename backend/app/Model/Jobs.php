<?php
/**
 * Created by PhpStorm.
 * User: anshumanc
 * Date: 2019-04-05
 * Time: 13:32
 */

namespace App\Model;


use Illuminate\Database\Eloquent\Model;

class Jobs extends Model
{
    protected $fillable = ["title", "description", "location", "category", "active"];

}