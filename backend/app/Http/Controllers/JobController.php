<?php
/**
 * Created by PhpStorm.
 * User: anshumanc
 * Date: 2019-04-04
 * Time: 17:54
 */

namespace App\Http\Controllers;


use App\Repositories\JobsRepository;

class JobController extends Controller
{
    /**
     * @var
     */
    public $repo;

    const MODEL = "App\Model\Jobs";

    use RESTActions;

    public function __construct(JobsRepository $jobsRepository)
    {
        $this->repo = $jobsRepository;
    }

}